import requests

BASE = "https://ki-gateway.tech4comp.dbis.rwth-aachen.de/lrs-requests-server"
# BASE = "http://127.0.0.1:12345"


student_id = "sha2561BRU9VNmDpdrBIBK5ea4939e5889222fc654d29a5786e4458d19dd828d3dc9d8e7b7de5898417453"
category_name = "math"
category_level = "2"

json_dict = {
    "student-id": student_id,
    "category-name": category_name,
    "category-level": category_level
}

response = requests.post(BASE + "/post-category-level", data=None, json=json_dict, verify=False)
print(response.content)

response = requests.get(BASE + f"/get-category-level?student-id={student_id}&category-name={category_name}", verify=False)
print(response.url)
print(response.content)


