from tincan import (RemoteLRS, Statement, Agent, Verb, Activity, Context, LanguageMap, ActivityDefinition, Extensions)
import lrs_properties


class LRSHandlerStudentLevelInCategory:
    def __init__(self):
        # set lrs connection
        self.lrs = RemoteLRS(
            version=lrs_properties.version,
            endpoint=lrs_properties.endpoint,
            username=lrs_properties.username,
            password=lrs_properties.password,
        )

        # xApi statement attributes
        self.verb_rated = ("rated", "http://id.tincanapi.com/verb/rated")
        self.language_map = "en-US"

    def get_latest_category_level_of_user(self, user_id, category_name):

        # query
        q_verb = Verb(
            id=self.verb_rated[1],
            display=LanguageMap({self.language_map: self.verb_rated[0]}),
        )
        q_actor = Agent(
            name=user_id,
            mbox=f"mailto:{user_id}@{lrs_properties.website_name}",
        )
        query = {
            "verb": q_verb,
            "actor": q_actor,
            "limit": 100,
        }

        # extract category level
        lrs_response = self.lrs.query_statements(query)
        statements_result = lrs_response.content
        latest_statement = self.get_latest_statement_of_user(statements_result, user_id)
        # latest_statement = statements_result.statements[0]
        extensions_key = f"{lrs_properties.uri}context/extensions/{category_name}-level"
        latest_category_level = int(latest_statement.context.extensions[extensions_key])

        return latest_category_level

    def get_latest_statement_of_user(self, statements_result, user_id):
        """
        Helper function, because the query cannot find the requested user but returns all 100 statements, which means
        out of those found statements we need to find the user.
        """
        found_statements = statements_result.statements
        print(len(found_statements))
        i = 0
        for s in found_statements:
            i += 1
            print(i, s.actor.name)
            if s.actor.name == user_id:
                return s

    def set_latest_category_level_of_user(self, studyId, category_name, category_level):
        user = str(studyId)

        # construct the statement
        actor = Agent(
            name=user,
            mbox=f"mailto:{user}@{lrs_properties.website_name}",
        )
        verb = Verb(
            id=self.verb_rated[1],
            display=LanguageMap({self.language_map: self.verb_rated[0]}),
        )
        object = Activity(
            id=f"{lrs_properties.uri}activities/onyx-task-adaptation",
            definition=ActivityDefinition(
                name=LanguageMap({self.language_map: "onyx-task-adaptation"}),
                description=LanguageMap({self.language_map: "This statement describes the level of a certain category the user has."}),
            ),
        )
        extensions_dict = {
            f"{lrs_properties.uri}context/extensions/{category_name}-level": str(category_level)
        }
        extensions = Extensions(extensions_dict)
        context = Context(
            extensions=extensions
        )

        # construct the actual statement
        statement = Statement(
            actor=actor,
            verb=verb,
            object=object,
            context=context,
        )

        # save our statement to the remote_lrs and store the response in "response"
        response = self.lrs.save_statement(statement)
        if not response:
            raise ValueError("statement failed to save")

        # retrieve our statement from the remote_lrs using the id returned in the response
        response = self.lrs.retrieve_statement(response.content.id)
        if not response.success:
            raise ValueError("statement could not be retrieved")

        # retrieve our statement from the remote_lrs using the id returned in the response
        response = self.lrs.retrieve_statement(response.content.id)

        if not response.success:
            raise ValueError("statement could not be retrieved")

        return f"A new statement has been created successfully on the learning record store. The level for the user \"{studyId}\" in the category \"{category_name}\" has been set to \"{str(category_level)}\"."

