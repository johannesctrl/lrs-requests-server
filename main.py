from flask import Flask
from flask import request
import json
import flask_cors
import sys

import lrs_handler_student_level_in_category
from lrs_handler import LrsHandler

app = Flask(__name__)
cors = flask_cors.CORS(app, resources={r"/api/*": {"origins": "*"}})


@app.route('/get-category-level', methods=["GET"])
@flask_cors.cross_origin()
def get_category_level():
    print("get_category_level", file=sys.stderr)

    # get arguments
    student_id = request.args.get("student-id")
    category_name = request.args.get("category-name")
    print(student_id, category_name)

    # get category level
    lrs_handler = lrs_handler_student_level_in_category.LRSHandlerStudentLevelInCategory()
    category_level = lrs_handler.get_latest_category_level_of_user(student_id, category_name)
    response_body = json.dumps({category_name: category_level})

    return response_body


@app.route('/post-category-level', methods=["POST"])
@flask_cors.cross_origin()
def post_category_level():
    print("post_category_level", file=sys.stderr)

    # get arguments
    data = json.loads(request.data)
    print(data)
    student_id = data["student-id"]
    category_name = data["category-name"]
    category_level = data["category-level"]

    # get category level
    lrs_handler = lrs_handler_student_level_in_category.LRSHandlerStudentLevelInCategory()
    response = lrs_handler.set_latest_category_level_of_user(student_id, category_name, category_level)

    return response


@app.route('/get-check-connection', methods=["GET"])
@flask_cors.cross_origin()
def get_check_connection():
    print("get_check_connection", file=sys.stderr)
    return "connected"


@app.route('/onyx-interaction-tracker/post-statement', methods=["POST"])
@flask_cors.cross_origin()
def post_onyx_interaction_tracker_statement():
    print("post_onyx_interaction_tracker_statement", file=sys.stderr)

    # get arguments
    data = json.loads(request.data)
    print(data)

    # post data to lrs
    lrs_handler = LrsHandler()
    response = lrs_handler.post_onyx_interaction_tracker_statement(data)

    return response


@app.route('/eye-focus-task-tracker/post-statement', methods=["POST"])
@flask_cors.cross_origin()
def post_eye_focus_task_tracker_statement():
    print("post_eye_focus_task_tracker_statement", file=sys.stderr)

    # get arguments
    data = json.loads(request.data)
    print(data, file=sys.stderr)

    # post data to lrs
    lrs_handler = LrsHandler()
    response = lrs_handler.post_eye_focus_task_tracker_statement(data)

    return response


# a test to see if the service is online
@app.route('/hello-world', methods=["GET"])
def hello_world():
    print("hello_world", file=sys.stderr)
    return "Hello World!"


if __name__ == "__main__":
    app.run(debug=False, host="0.0.0.0", port=12345)
    # app.run(debug=True, port=12345)
