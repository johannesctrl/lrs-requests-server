# lrs-requests-server
This project provides a flask server to process RESTful-Api requests to get information from an LRS with help of the TinCanPy-library.

## REST API calls
The service provides the following api calls.

|request method|url suffix|description|
|---|---|---|
|POST|/lrs-requests-server/get-category-level|Get the category level of the student from the LRS by giving the userId and the category name.|
|POST|/lrs-requests-server/post-category-level|Posts a category level to the LRS by giving level and the user id.|