import sys

from tincan import (RemoteLRS, Statement, Agent, Verb, Activity, Context, LanguageMap, ActivityDefinition, Extensions)
import lrs_properties


class LrsHandler:
    def __init__(self):
        # set lrs connection
        self.lrs = RemoteLRS(
            version=lrs_properties.version,
            endpoint=lrs_properties.endpoint,
            username=lrs_properties.username,
            password=lrs_properties.password,
        )

        # xApi statement attributes
        self.verb_evaluated = ("evaluated", "http://www.tincanapi.co.uk/verbs/evaluated")
        self.language_map = "en-US"

    def post_onyx_interaction_tracker_statement(self, json_data):

        # extract data from json
        user_id = json_data["userId"]
        study_name = json_data["studyName"]
        fragebogen_title = json_data["fragebogenTitle"]
        task_id = json_data["taskId"]
        task_title = json_data["taskTitle"]
        task_type_index = json_data["taskTypeIndex"]
        task_element_id = json_data["taskElementId"]
        input_field_index = json_data["inputFieldIndex"]
        task_input_value = json_data["taskInputValue"]
        input_counter = json_data["inputCounter"]
        timestamp = json_data["timestamp"]
        start_timestamp = json_data["startTimestamp"]
        json_version = json_data["jsonVersion"]

        # construct the statement
        actor = Agent(
            name=user_id,
            mbox=f"mailto:{user_id}@{lrs_properties.website_name}",
        )
        verb = Verb(
            id=self.verb_evaluated[1],
            display=LanguageMap({self.language_map: self.verb_evaluated[0]}),
        )
        object = Activity(
            id=f"{lrs_properties.uri}activities/{study_name}-study",
            definition=ActivityDefinition(
                name=LanguageMap({self.language_map: study_name}),
                description=LanguageMap({self.language_map: "This statement describes an interaction input of an onyx task."}),
            ),
        )
        extensions_dict = {
            f"{lrs_properties.uri}context/extensions/fragebogenTitle": str(fragebogen_title),
            f"{lrs_properties.uri}context/extensions/taskId": str(task_id),
            f"{lrs_properties.uri}context/extensions/taskTitle": str(task_title),
            f"{lrs_properties.uri}context/extensions/taskTypeIndex": str(task_type_index),
            f"{lrs_properties.uri}context/extensions/taskElementId": str(task_element_id),
            f"{lrs_properties.uri}context/extensions/inputFieldIndex": str(input_field_index),
            f"{lrs_properties.uri}context/extensions/taskInputValue": str(task_input_value),
            f"{lrs_properties.uri}context/extensions/inputCounter": str(input_counter),
            f"{lrs_properties.uri}context/extensions/timestamp": str(timestamp),
            f"{lrs_properties.uri}context/extensions/startTimestamp": str(start_timestamp),
            f"{lrs_properties.uri}context/extensions/jsonVersion": str(json_version)
        }
        extensions = Extensions(extensions_dict)
        context = Context(
            extensions=extensions
        )

        # construct the actual statement
        statement = Statement(
            actor=actor,
            verb=verb,
            object=object,
            context=context,
        )

        # save our statement to the remote_lrs and store the response in "response"
        response = self.lrs.save_statement(statement)
        if not response:
            raise ValueError("statement failed to save")

        # retrieve our statement from the remote_lrs using the id returned in the response
        response = self.lrs.retrieve_statement(response.content.id)
        if not response.success:
            raise ValueError("statement could not be retrieved")

        # retrieve our statement from the remote_lrs using the id returned in the response
        response = self.lrs.retrieve_statement(response.content.id)

        if not response.success:
            raise ValueError("statement could not be retrieved")

        return f"A new statement has been created successfully on the learning record store. The interaction input of the user \"{user_id}\" has been posted."

    def post_eye_focus_task_tracker_statement(self, json_data):

        # extract data from json
        user_id = json_data["userId"]
        study_name = json_data["studyName"]
        task_id = json_data["taskId"]
        task_element_id = json_data["taskElementId"]
        timestamp = json_data["timestamp"]
        start_timestamp = json_data["startTimestamp"]

        # construct the statement
        actor = Agent(
            name=user_id,
            mbox=f"mailto:{user_id}@{lrs_properties.website_name}",
        )
        verb = Verb(
            id=self.verb_evaluated[1],
            display=LanguageMap({self.language_map: self.verb_evaluated[0]}),
        )
        object = Activity(
            id=f"{lrs_properties.uri}activities/{study_name}-study",
            definition=ActivityDefinition(
                name=LanguageMap({self.language_map: study_name}),
                description=LanguageMap({self.language_map: "This statement describes if a person is looking at a task."}),
            ),
        )
        extensions_dict = {
            f"{lrs_properties.uri}context/extensions/taskId": str(task_id),
            f"{lrs_properties.uri}context/extensions/taskElementId": str(task_element_id),
            f"{lrs_properties.uri}context/extensions/timestamp": str(timestamp),
            f"{lrs_properties.uri}context/extensions/startTimestamp": str(start_timestamp),
        }
        extensions = Extensions(extensions_dict)
        context = Context(
            extensions=extensions
        )

        # construct the actual statement
        statement = Statement(
            actor=actor,
            verb=verb,
            object=object,
            context=context,
        )

        # save our statement to the remote_lrs and store the response in "response"
        response = self.lrs.save_statement(statement)
        if not response:
            raise ValueError("statement failed to save")

        # retrieve our statement from the remote_lrs using the id returned in the response
        response = self.lrs.retrieve_statement(response.content.id)
        if not response.success:
            raise ValueError("statement could not be retrieved")

        # retrieve our statement from the remote_lrs using the id returned in the response
        response = self.lrs.retrieve_statement(response.content.id)

        if not response.success:
            raise ValueError("statement could not be retrieved")

        return f"A new statement has been created successfully on the learning record store. The interaction input of the user \"{user_id}\" has been posted."
