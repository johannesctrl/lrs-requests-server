import os
from dotenv import load_dotenv

load_dotenv()
endpoint = os.getenv("LRS_ENDPOINT")
version = "1.0.3"
username = os.getenv("LRS_CLIENT_KEY")
password = os.getenv("LRS_CLIENT_SECRET")

uri = os.getenv("LRS_URI")
website_name = os.getenv("LRS_WEBSITE_NAME")